
DESCRIPTION
===========

Sift Science (https://siftscience.com/) integration for the Drupal Commerce
payment and checkout system.

Supports:

* Tracking User Activity

  Installs a JavaScript snippet on all customer-facing parts of your site
  (all paths outside of administrative section of the site).

  Additionally sends user activity data using Sift Science's REST API.
  Handles the following events:
  - $create_account
  - $update_account
  - $login
  - $logout

  More info:
  - https://siftscience.com/resources/guides/chargebacks#js-snippet
  - https://siftscience.com/resources/references/events-api

* Checkout

  Sends order and transaction data using Sift Science's REST API.

  Handles the following events:
  - $create_order
  - $transaction
  - $add_item_to_cart
  - $remove_item_from_cart

  More info:
  - https://siftscience.com/resources/guides/chargebacks#checkout
  - https://siftscience.com/resources/references/events-api

* Decision

  Displays user Sift Score, label (if set) and recommended order action
  on order view and edit pages, assisting site admins with decision whether
  the order should be accepted and processed or not.

  More info:
  - https://siftscience.com/resources/guides/chargebacks#decision-flow
  - https://siftscience.com/resources/references/score-api

* Feedback

  Allows site admins/users with relevant permission to label customers
  as 'bad' or 'not bad'.

  More info:
  - https://siftscience.com/resources/guides/chargebacks#feedback
  - https://siftscience.com/resources/references/labels-api



INSTALLATION
============

1.  Place the module into your modules directory.
    This is normally the 'sites/all/modules' directory.

2.  Go to admin/build/modules. Enable the module.
    The modules is found in the 'Commerce (contrib)' section.

3. Go to Administration » Store » Configuration » Sift Science settings
   (admin/commerce/config/siftscience) to configure the module:

   * enable required integrations (you can find more information on each one
     at https://siftscience.com/resources/guides/chargebacks)

   * provide your Javascript snippet key and REST API key values - you can
     find them in your Sift Science account under Developer » API Keys
     (https://siftscience.com/console/developer/api-keys)



COMPLEMENTARY MODULES
=====================

* Commerce Sift Science Stripe
  Adds Commerce Stripe support to Commerce Sift Science.
  @TODO: URL here.



FURTHER READING
===============

* Ecommerce Fraud Prevention Guides and Resources
  https://siftscience.com/sift-edu
