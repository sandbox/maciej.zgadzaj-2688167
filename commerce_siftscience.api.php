<?php

/**
 * @file
 * API documentation for Commerce Sift Science.
 */

/**
 * Change the '$create_order' request data before it is sent to Sift Science API.
 *
 * @param array $request_data
 *   An array of request data for '$create_order' API call.
 * @param object $order
 *   The order object that will be sent to Sift Science.
 *
 * @see commerce_siftscience_checkout_create_order_get_request_data()
 */
function hook_commerce_siftscience_event_create_order_alter(&$request_data, $order) {
  // See commerce_siftscience_stripe module for example.
}

/**
 * Change the '$transaction' request data before it is sent to Sift Science API.
 *
 * @param array $request_data
 *   An array of request data for '$transaction' API call.
 * @param object $transaction
 *   The payment transaction object that will be sent to Sift Science.
 * @param object $order
 *   The order object which the payment transaction was created for.
 *
 * @see commerce_siftscience_checkout_transaction_get_request_data()
 */
function hook_commerce_siftscience_event_transaction_alter(&$request_data, $transaction, $order) {
  // See commerce_siftscience_stripe module for example.
}

/**
 * Change the '$add_item_to_cart' request data before it is sent to Sift
 * Science API.
 *
 * @param array $request_data
 *   An array of request data for '$add_item_to_cart' API call.
 * @param object $order
 *   The cart order object the product was added to.
 * @param object $product
 *   The product that was added to the cart.
 * @param int $quantity
 *   The quantity of the product added to the cart.
 * @param object $line_item
 *   The new or updated line item representing that product on the given order.
 */
function hook_commerce_siftscience_event_add_item_to_cart_alter(&$request_data, $order, $product, $quantity, $line_item) {
  // TBC.
}

/**
 * Change the '$remove_item_from_cart' request data before it is sent to Sift
 * Science API.
 *
 * @param array $request_data
 *   An array of request data for '$remove_item_from_cart' API call.
 * @param object $order
 *   The cart order object the product was removed from.
 * @param object $product
 *   The product that was removed from the cart.
 * @param int $quantity
 *   The quantity of the product line item removed from the cart.
 * @param object $line_item
 *   The product line item that was deleted to remove the product from the cart.
 */
function hook_commerce_siftscience_event_remove_item_from_cart_alter(&$request_data, $order, $product, $quantity, $line_item) {
  // TBC.
}

/**
 * Change the '$create_account' request data before it is sent to Sift Science
 * API.
 *
 * @param array $request_data
 *   An array of request data for '$create_account' API call.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function hook_commerce_siftscience_event_create_account_alter(&$request_data, $account) {
  // TBC.
}

/**
 * Change the '$update_account' request data before it is sent to Sift Science
 * API.
 *
 * @param array $request_data
 *   An array of request data for '$update_account' API call.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function hook_commerce_siftscience_event_update_account_alter(&$request_data, $account) {
  // TBC.
}

/**
 * Change the '$login' request data before it is sent to Sift Science API.
 *
 * @param array $request_data
 *   An array of request data for '$login' API call.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function hook_commerce_siftscience_event_login_alter(&$request_data, $account) {
  // TBC.
}

/**
 * Change the '$logout' request data before it is sent to Sift Science API.
 *
 * @param array $request_data
 *   An array of request data for '$logout' API call.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function hook_commerce_siftscience_event_logout_alter(&$request_data, $account) {
  // TBC.
}

/**
 * Change the user label request data before it is sent to Sift Science API.
 *
 * @param array $request_data
 *   An array of request data for '$logout' API call.
 * @param array $form_values
 *   An array of values submitted in user labelling form.
 */
function hook_commerce_siftscience_label_user_alter(&$request_data, $form_values) {
  // TBC.
}
