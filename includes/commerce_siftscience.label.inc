<?php

/**
 * @file
 * Label API integration for the Sift Science module.
 *
 * @see https://siftscience.com/resources/guides/chargebacks#decision-flow
 * @see https://siftscience.com/resources/references/labels-api
 */

/**
 *
 *
 * @see commerce_siftscience_label_user_form_submit()
 */
function commerce_siftscience_label_user_form($form, $form_state, $account) {
//  $form += commerce_siftscience_get_score_element($account->uid);
  drupal_set_title(t('Label user @name', array('@name' => $account->name)));

  $form['account'] = array(
    '#type' => 'value',
    '#value' => $account,
  );

  $form['is_bad'] = array(
    '#type' => 'select',
    '#title' => t('Label'),
    '#description' => t('Indicate whether this user is fraudulent, or otherwise engaging in activities that are negative for your business.'),
    '#options' => commerce_siftscience_label_get_labels(),
    '#required' => TRUE,
  );
  if (arg(3) !== NULL && in_array(arg(3), array_keys(commerce_siftscience_label_get_labels()))) {
    $form['is_bad']['#default_value'] = arg(3);
  }

  $form['reasons'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Reasons'),
    '#description' => t('A list of one or more reasons indicating why this particular user has been labeled "bad". Note: while reasons are useful, they are not required.'),
    '#options' => commerce_siftscience_label_get_reasons(),
    '#states' => array(
      'visible' => array(
        ':input[name="is_bad"]' => array('value' => '1'),
      ),
    ),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Description of the user and/or incident triggering the label for your internal record-keeping.'),
    '#rows' => 3,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit label'),
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 *
 * @see function commerce_siftscience_label_user_form()
 */
function commerce_siftscience_label_user_form_submit($form, &$form_state) {
  $labels = commerce_siftscience_label_get_labels();

  $request_data = commerce_siftscience_label_user_get_request_data($form_state['values']);
  $response = commerce_siftscience_api_call('label', $form_state['account']->uid, $request_data);

  if ($response->isOk()) {
    drupal_set_message(t('User %name has been labelled successfully as %label.', array(
      '%name' => $form_state['account']->name,
      '%label' => $labels[$form_state['is_bad']],
    )));
  }
  else {
    drupal_set_message(t('There was an error labelling user %name as %label.', array(
      '%name' => $form_state['account']->name,
      '%label' => $labels[$form_state['is_bad']],
    )), 'error');
  }
}

function commerce_siftscience_label_user_get_request_data($form_values) {
  $request_data = array(
    '$is_bad' => $form_values['is_bad'],
    '$reasons' => $form_values['reasons'],
    '$description' => $form_values['description'],
    '$source' => t('Manual review'),
    '$analyst' => $GLOBALS['user']->uid,
  );

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_label_user', $request_data, $form_values);

  return $request_data;
}

/**
 * Menu callback: unlabel a user.
 *
 * @param object $account
 *
 * @see commerce_siftscience_menu()
 */
function commerce_siftscience_label_user_unlabel($account) {
  $response = commerce_siftscience_api_call('unlabel', $account->uid);
  if ($response->isOk()) {
    drupal_set_message(t('User %name has been unlabelled successfully.', array(
      '%name' => $account->name,
    )));
  }
  else {
    drupal_set_message(t('There was an error unlabelling user %name.', array(
      '%name' => $account->name,
    )), 'error');
  }
}

function commerce_siftscience_label_get_labels() {
  return array(
    TRUE => 'bad',
    FALSE => 'not bad',
  );
}

function commerce_siftscience_label_get_reasons() {
  return array(
    '$chargeback' => t('Chargeback received for this user.'),
    '$spam' => t('The user is sending spam.'),
    '$funneling' => t('The user engaged in coordinated buying and selling in an effort to funnel money.'),
    '$fake' => t('The user is listing non-existent or fraudulent products or services for sale.'),
    '$referral' => t('The user engaged in various kinds of referral abuse.'),
    '$duplicate_account' => t('This user account is a duplicate of another account.'),
  );
}
