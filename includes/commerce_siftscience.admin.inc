<?php

/**
 * @file
 * Administration page callbacks for the Sift Science module.
 */

/**
 * Form constructor for the Sift Science settings form.
 *
 * @see commerce_siftscience_menu()
 *
 * @ingroup forms
 */
function commerce_siftscience_admin_settings_form() {
  $form = array();

  $form['commerce_siftscience_integration'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Integration'),
    '#options' => array(
      'user_activity' => t('User activity'),
      'checkout' => t('Checkout'),
      'decision' => t('Decision'),
      'feedback' => t('Feedback'),
    ),
    '#default_value' => variable_get('commerce_siftscience_integration', array()),
  );

  $form['commerce_siftscience_js_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Javascript snippet key'),
    '#description' => l(t('Need help finding your API key?'), 'https://support.siftscience.com/hc/en-us/articles/202282246-How-do-I-find-my-API-keys-', array('external' => TRUE)),
    '#default_value' => variable_get('commerce_siftscience_js_key'),
  );

  $form['commerce_siftscience_rest_key'] = array(
    '#type' => 'textfield',
    '#title' => t('REST API key'),
    '#description' => l(t('Need help finding your API key?'), 'https://support.siftscience.com/hc/en-us/articles/202282246-How-do-I-find-my-API-keys-', array('external' => TRUE)),
    '#default_value' => variable_get('commerce_siftscience_rest_key'),
  );

  $form['commerce_siftscience_api_logging'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => variable_get('commerce_siftscience_api_logging', array()),
  );

  return system_settings_form($form);
}
