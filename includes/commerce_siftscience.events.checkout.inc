<?php

/**
 * @file
 * Checkout integration for the Sift Science module.
 *
 * @see https://siftscience.com/resources/guides/chargebacks#checkout
 */

/**
 * Implements hook_commerce_checkout_complete().
 *
 * Sends the completed order to Sift Science.
 */
function commerce_siftscience_commerce_checkout_complete($order) {
  // Send completed order to Sift Science only if 'Checkout' integration
  // is enabled and 'REST API key' is provided.
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['checkout']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_create_order($order);
  }
}

/**
 * Implements hook_entity_update().
 *
 * Sends the completed transaction to Sift Science.
 */
function commerce_siftscience_entity_update($entity, $type) {
  // Process only payment transactions that have remote_id value set,
  // which means they were processed by the payment gateway.
  if ($type == 'commerce_payment_transaction' && !empty($entity->remote_id)) {
    // Send transaction to Sift Science only if 'Checkout' integration
    // is enabled and 'REST API key' is provided.
    $integration = variable_get('commerce_siftscience_integration');
    if (!empty($integration['checkout']) && variable_get('commerce_siftscience_rest_key')) {
      commerce_siftscience_event_transaction($entity);
    }
  }
}

/**
 * Implements hook_commerce_cart_product_add().
 *
 * Sends the product added to cart notification to Sift Science.
 */
function commerce_siftscience_commerce_cart_product_add($order, $product, $quantity, $line_item) {
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['checkout']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_add_item_to_cart($order, $product, $quantity, $line_item);
  }
}

/**
 * Implements hook_commerce_cart_product_remove().
 *
 * Sends the product removed from cart notification to Sift Science.
 */
function commerce_siftscience_commerce_cart_product_remove($order, $product, $quantity, $line_item) {
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['checkout']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_remove_item_from_cart($order, $product, $quantity, $line_item);
  }
}

/**
 * Sends the completed order to Sift Science using '$create_order' API event.
 *
 * @param object $order
 *   The order object that will be sent to Sift Science.
 *
 * @see commerce_siftscience_commerce_checkout_complete()
 * @see https://siftscience.com/resources/references/events-api#event-create-order
 */
function commerce_siftscience_event_create_order($order) {
  $request_data = commerce_siftscience_event_create_order_get_request_data($order);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$create_order' API call.
 *
 * @param object $order
 *   The order object that will be sent to Sift Science.
 *
 * @return array
 *   An array of request data for '$create_order' API call.
 *
 * @see commerce_siftscience_event_create_order()
 * @see https://siftscience.com/resources/references/events-api#event-create-order
 */
function commerce_siftscience_event_create_order_get_request_data($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $request_data = array(
    '$type' => '$create_order',
    '$user_id' => $order->uid,
    '$session_id' => session_id(),
    '$order_id' => $order->order_number,
    '$user_email' => $order->mail,
    // Total transaction amount in micros (1 cent = 10000 micros).
    '$amount' => $order_wrapper->commerce_order_total->amount->value() * 10000,
    '$currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
    '$items' => array(),
    '$payment_methods' => array(),
    '$billing_address' => commerce_siftscience_get_address($order_wrapper, 'billing'),
    '$shipping_address' => commerce_siftscience_get_address($order_wrapper, 'shipping'),
  );

  foreach ($order_wrapper->commerce_line_items as $commerce_line_item_wrapper) {
    if (in_array($commerce_line_item_wrapper->type->value(), commerce_product_line_item_types())) {
      $request_data['$items'][] = commerce_siftscience_get_item($commerce_line_item_wrapper);
    }
  }

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_create_order', $request_data, $order);

  return $request_data;
}

/**
 * Sends the completed transaction to Sift Science using '$transaction' API event.
 *
 * @param object $transaction
 *   The transaction object that will be sent to Sift Science.
 *
 * @see commerce_siftscience_entity_update()
 * @see https://siftscience.com/resources/references/events-api#event-transaction
 */
function commerce_siftscience_event_transaction($transaction) {
  $request_data = commerce_siftscience_event_transaction_get_request_data($transaction);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$transaction' API call.
 *
 * @param object $transaction
 *   The transaction object that will be sent to Sift Science.
 *
 * @return array
 *   An array of request data for '$transaction' API call.
 *
 * @see commerce_siftscience_event_transaction()
 * @see https://siftscience.com/resources/references/events-api#event-transaction
 */
function commerce_siftscience_event_transaction_get_request_data($transaction) {
  $order = commerce_order_load($transaction->order_id);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $request_data = array(
    '$type' => '$transaction',
    '$user_id' => $transaction->uid,
    // Total transaction amount in micros (1 cent = 10000 micros).
    '$amount' => $transaction->amount * 10000,
    '$currency_code' => $transaction->currency_code,
    '$user_email' => $order->mail,
    '$order_id' => $order->order_number,
    '$transaction_id' => $transaction->transaction_id,
    '$payment_method' => array(),
    '$billing_address' => commerce_siftscience_get_address($order_wrapper, 'billing'),
    '$shipping_address' => commerce_siftscience_get_address($order_wrapper, 'shipping'),
    '$session_id' => session_id(),
  );

  switch ($transaction->status) {
    case COMMERCE_PAYMENT_STATUS_SUCCESS:
      $request_data['$transaction_status'] = '$success';
      break;
    case COMMERCE_PAYMENT_STATUS_FAILURE:
      $request_data['$transaction_status'] = '$failure';
      break;
    case COMMERCE_PAYMENT_STATUS_PENDING:
      $request_data['$transaction_status'] = '$pending';
      break;
  }

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_transaction', $request_data, $transaction, $order);

  return $request_data;
}

/**
 * Sends the notification to Sift Science using '$add_item_to_cart' API event.
 *
 * @param object $order
 *   The cart order object the product was added to.
 * @param object $product
 *   The product that was added to the cart.
 * @param int $quantity
 *   The quantity of the product added to the cart.
 * @param object $line_item
 *   The new or updated line item representing that product on the given order.
 *
 * @see commerce_siftscience_commerce_cart_product_add()
 * @see https://siftscience.com/resources/references/events-api#event-add-item-to-cart
 */
function commerce_siftscience_event_add_item_to_cart($order, $product, $quantity, $line_item) {
  $request_data = commerce_siftscience_event_add_item_to_cart_get_request_data($order, $product, $quantity, $line_item);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$add_item_to_cart' API call.
 *
 * @param object $order
 *   The cart order object the product was added to.
 * @param object $product
 *   The product that was added to the cart.
 * @param int $quantity
 *   The quantity of the product added to the cart.
 * @param object $line_item
 *   The new or updated line item representing that product on the given order.
 *
 * @return array
 *   An array of request data for '$add_item_to_cart' API call.
 *
 * @see commerce_siftscience_event_add_item_to_cart()
 * @see https://siftscience.com/resources/references/events-api#event-add-item-to-cart
 */
function commerce_siftscience_event_add_item_to_cart_get_request_data($order, $product, $quantity, $line_item) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  $request_data = array(
    '$type' => '$add_item_to_cart',
    '$session_id' => session_id(),
    '$user_id' => $order->uid,
    '$item' => commerce_siftscience_get_item($line_item_wrapper, $quantity),
  );

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_add_item_to_cart', $request_data, $order, $product, $quantity, $line_item);

  return $request_data;
}

/**
 * Sends the notification to Sift Science using '$remove_item_from_cart' API event.
 *
 * @param object $order
 *   The cart order object the product was removed from.
 * @param object $product
 *   The product that was removed from the cart.
 * @param int $quantity
 *   The quantity of the product line item removed from the cart.
 * @param object $line_item
 *   The product line item that was deleted to remove the product from the cart.
 *
 * @see commerce_siftscience_commerce_cart_product_remove()
 * @see https://siftscience.com/resources/references/events-api#event-remove-item-from-cart
 */
function commerce_siftscience_event_remove_item_from_cart($order, $product, $quantity, $line_item) {
  $request_data = commerce_siftscience_event_remove_item_from_cart_get_request_data($order, $product, $quantity, $line_item);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$remove_item_from_cart' API call.
 *
 * @param object $order
 *   The cart order object the product was removed from.
 * @param object $product
 *   The product that was removed from the cart.
 * @param int $quantity
 *   The quantity of the product line item removed from the cart.
 * @param object $line_item
 *   The product line item that was deleted to remove the product from the cart.
 *
 * @return array
 *   An array of request data for '$remove_item_from_cart' API call.
 *
 * @see commerce_siftscience_event_remove_item_from_cart()
 * @see https://siftscience.com/resources/references/events-api#event-remove-item-from-cart
 */
function commerce_siftscience_event_remove_item_from_cart_get_request_data($order, $product, $quantity, $line_item) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  $request_data = array(
    '$type' => '$remove_item_from_cart',
    '$session_id' => session_id(),
    '$user_id' => $order->uid,
    '$item' => commerce_siftscience_get_item($line_item_wrapper, $quantity),
  );

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_remove_item_from_cart', $request_data, $order, $product, $quantity, $line_item);

  return $request_data;
}

/**
 * Returns an array of address data in Sift Science API's 'Address' format.
 *
 * @param object $order_wrapper
 *   The wrapped order object to get the address details from.
 * @param string $profile_type
 *   Customer profile type to get the address details for.
 *
 * @return array
 *   An array of address data in Sift Science API's 'Address' format.
 *
 * @see https://siftscience.com/resources/references/events-api#complex-fields-address
 */
function commerce_siftscience_get_address($order_wrapper, $profile_type) {
  $address_data = array();

  $profile_field_name = 'commerce_customer_' . $profile_type;
  if (
    $order_wrapper->__isset($profile_field_name)
    && $order_wrapper->$profile_field_name->__isset('commerce_customer_address')
  ) {
    $customer_address = $order_wrapper->$profile_field_name->commerce_customer_address->value();
    $address_data = array(
      '$name' => $customer_address['name_line'],
      '$address_1' => $customer_address['thoroughfare'],
      '$address_2' => $customer_address['premise'],
      '$zipcode' => $customer_address['postal_code'],
      '$city' => $customer_address['locality'],
      '$region' => $customer_address['administrative_area'],
      '$country' => $customer_address['country'],
      '$phone' => $customer_address['phone_number'],
    );
  }

  return $address_data;
}

/**
 * Returns an array of product data in Sift Science API's 'Item' format.
 *
 * @param object $line_item_wrapper
 *   The wrapped line item object to get the product details from.
 *
 * @return array
 *   An array of product data in Sift Science API's 'Item' format.
 *
 * @see https://siftscience.com/resources/references/events-api#complex-fields-item
 */
function commerce_siftscience_get_item($line_item_wrapper, $quantity = NULL) {
  $product_data = array();

  if ($line_item_wrapper->__isset('commerce_product')) {
    $product = $line_item_wrapper->commerce_product->value();
    $product_data = array(
      '$item_id' => $product->product_id,
      '$product_title' => $product->title,
      // The item unit price in micros (1 cent = 10000 micros).
      '$price' => $line_item_wrapper->commerce_unit_price->amount->value() * 10000,
      '$currency_code' => $line_item_wrapper->commerce_unit_price->currency_code->value(),
      '$quantity' => (int) ((!empty($quantity)) ? $quantity : $line_item_wrapper->quantity->value()),
      '$sku' => $product->sku,
    );
  }

  return $product_data;
}
