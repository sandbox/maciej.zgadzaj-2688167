<?php

/**
 * @file
 * User integration for the Sift Science module.
 */

/**
 * Implements hook_user_insert().
 */
function commerce_siftscience_user_insert(&$edit, $account, $category) {
  // Send new user information to Sift Science only if 'User activity'
  // integration is enabled and 'REST API key' is provided.
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['user_activity']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_create_account($account);
  }
}

/**
 * Implements hook_user_update().
 */
function commerce_siftscience_user_update(&$edit, $account, $category) {
  // Send updated user information to Sift Science only if 'User activity'
  // integration is enabled and 'REST API key' is provided.
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['user_activity']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_update_account($account);
  }
}

/**
 * Implements hook_user_login().
 */
function commerce_siftscience_user_login(&$edit, $account) {
  // Send login information to Sift Science only if 'User activity'
  // integration is enabled and 'REST API key' is provided.
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['user_activity']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_login($account);
  }
}

/**
 * Implements hook_user_logout().
 */
function commerce_siftscience_user_logout($account) {
  // Send logout information to Sift Science only if 'User activity'
  // integration is enabled and 'REST API key' is provided.
  $integration = variable_get('commerce_siftscience_integration');
  if (!empty($integration['user_activity']) && variable_get('commerce_siftscience_rest_key')) {
    commerce_siftscience_event_logout($account);
  }
}

/**
 * Sends the new user information to Sift Science using '$create_account'
 * API event.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_user_insert()
 * @see https://siftscience.com/resources/references/events-api#event-create-account
 */
function commerce_siftscience_event_create_account($account) {
  $request_data = commerce_siftscience_event_create_account_get_request_data($account);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$create_account' API call.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_event_create_account()
 * @see https://siftscience.com/resources/references/events-api#event-create-account
 */
function commerce_siftscience_event_create_account_get_request_data($account) {
  $request_data = array(
    '$type' => '$create_account',
    '$user_id' => $account->uid,
    '$session_id' => session_id(),
    '$user_email' => $account->mail,
  );

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_create_account', $request_data, $account);

  return $request_data;
}

/**
 * Sends the updated user information to Sift Science using '$update_account'
 * API event.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_user_update()
 * @see https://siftscience.com/resources/references/events-api#event-update-account
 */
function commerce_siftscience_event_update_account($account) {
  $request_data = commerce_siftscience_event_update_account_get_request_data($account);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$update_account' API call.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_event_update_account()
 * @see https://siftscience.com/resources/references/events-api#event-update-account
 */
function commerce_siftscience_event_update_account_get_request_data($account) {
  $request_data = array(
    '$type' => '$update_account',
    '$user_id' => $account->uid,
    '$user_email' => $account->mail,
  );

  if ($account->pass != $account->original->pass) {
    $request_data['$changed_password'] = TRUE;
  }

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_update_account', $request_data, $account);

  return $request_data;
}

/**
 * Sends the user login information to Sift Science using '$login' API event.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_user_login()
 * @see https://siftscience.com/resources/references/events-api#event-login
 */
function commerce_siftscience_event_login($account) {
  $request_data = commerce_siftscience_event_login_get_request_data($account);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$login' API call.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_event_login
 * @see https://siftscience.com/resources/references/events-api#event-login
 */
function commerce_siftscience_event_login_get_request_data($account) {
  // @TODO: Add support for failed logins.
  $request_data = array(
    '$type' => '$login',
    '$user_id' => $account->uid,
    '$session_id' => session_id(),
    '$login_status' => '$success',
  );

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_login', $request_data, $account);

  return $request_data;
}

/**
 * Sends the user logout information to Sift Science using '$logout' API event.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_user_logout()
 * @see https://siftscience.com/resources/references/events-api#event-logout
 */
function commerce_siftscience_event_logout($account) {
  $request_data = commerce_siftscience_event_logout_get_request_data($account);
  commerce_siftscience_api_call('track', $request_data);
}

/**
 * Builds an array of request data for '$logout' API call.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @see commerce_siftscience_event_logout()
 * @see https://siftscience.com/resources/references/events-api#event-logout
 */
function commerce_siftscience_event_logout_get_request_data($account) {
  $request_data = array(
    '$type' => '$logout',
    '$user_id' => $account->uid,
  );

  // Allow other modules to alter the $request_data array.
  drupal_alter('commerce_siftscience_event_logout', $request_data, $account);

  return $request_data;
}
