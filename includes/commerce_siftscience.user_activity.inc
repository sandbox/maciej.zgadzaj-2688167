<?php

/**
 * @file
 * User activity tracking integration for the Sift Science module.
 *
 * @see https://siftscience.com/resources/guides/chargebacks#js-snippet
 */

/**
 * Implements hook_page_alter().
 */
function commerce_siftscience_page_alter(&$page) {
  global $user;

  $integration = variable_get('commerce_siftscience_integration');

  if (
    // Include JS snippet only if 'User activity' integration is enabled.
    !empty($integration['user_activity'])
    // Include JS snippet only if 'Javascript snippet key' is provided.
    && ($js_snippet_key = variable_get('commerce_siftscience_js_key'))
    // JS snippet should not be included on internal tools (admin paths).
    // See https://siftscience.com/resources/guides/chargebacks#js-snippet
    && !path_is_admin(current_path())
  ) {
    $script = "
      var _user_id = " . (user_is_anonymous() ? "''" : $user->uid) . ";
      var _session_id = '" . session_id() . "';

      var _sift = window._sift = window._sift || [];
      _sift.push(['_setAccount', '" . $js_snippet_key . "']);
      _sift.push(['_setUserId', _user_id]);
      _sift.push(['_setSessionId', _session_id]);
      _sift.push(['_trackPageview']);

      (function() {
        function ls() {
          var e = document.createElement('script');
          e.type = 'text/javascript';
          e.async = true;
          e.src = ('https:' === document.location.protocol ? 'https://' : 'http://') + 'cdn.siftscience.com/s.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(e, s);
        }
        if (window.attachEvent) {
          window.attachEvent('onload', ls);
        } else {
          window.addEventListener('load', ls, false);
        }
      }());
    ";
    drupal_add_js($script, array('scope' => 'header', 'type' => 'inline'));
  }
}
