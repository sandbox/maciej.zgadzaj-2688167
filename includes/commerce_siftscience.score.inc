<?php

/**
 * @file
 * Score API integration for the Sift Science module.
 *
 * @see https://siftscience.com/resources/guides/chargebacks#decision-flow
 * @see https://siftscience.com/resources/references/score-api
 */

/**
 * Implements hook_commerce_order_view_alter().
 *
 * Displays Sift Science user score information on order view page.
 */
function commerce_siftscience_commerce_order_view_alter(&$build, $entity_type) {
  $integration = variable_get('commerce_siftscience_integration');
  if (
    !empty($integration['decision'])
    && variable_get('commerce_siftscience_rest_key')
    && user_access('access siftscience information')
  ) {
    $order = $build['#entity'];
    $build += commerce_siftscience_get_score_element($order->uid);
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for commerce_order_ui_order_form().
 *
 * Displays Sift Science user score information on order edit page.
 */
function commerce_siftscience_form_commerce_order_ui_order_form_alter(&$form, &$form_state) {
  $integration = variable_get('commerce_siftscience_integration');
  if (
    !empty($integration['decision'])
    && variable_get('commerce_siftscience_rest_key')
    && user_access('access siftscience information')
  ) {
    $order = $form['#entity'];
    $form += commerce_siftscience_get_score_element($order->uid);
  }
}

/**
 * Builds page header displaying Sift Science information about a user.
 *
 * @param object $order
 *   An order whose owner to display the user score information for.
 *
 * @return mixed
 *   A render array for displaying the user score information.
 */
function commerce_siftscience_get_score_element($uid) {
  $response = commerce_siftscience_api_call('score', $uid);

  // If we were not able to make the call for whatever reason (the response
  // is empty), just return nothing.
  if (!$response) {
    return array();
  }

  // Logic from Sift Science Decision Flow:
  // https://siftscience.com/resources/guides/chargebacks#decision-flow
  if ($response->isOk()) {
    if (isset($response->body['latest_label']['is_bad']) && $response->body['latest_label']['is_bad'] === TRUE) {
      $decision = 'reject';
    }
    elseif (isset($response->body['latest_label']['is_bad']) && $response->body['latest_label']['is_bad'] === FALSE) {
      $decision = 'accept';
    }
    elseif ($response->body['score'] >= 0.95) {
      $decision = 'reject';
    }
    elseif ($response->body['score'] >= .6 && $response->body['score'] < 0.95) {
      $decision = 'hold';
    }
    else {
      $decision = 'accept';
    }
  }
  else {
    $decision = 'reject';
  }

  $columns = array(
    array(
      'data' => l(t('Sift Science'), 'https://siftscience.com/console', array('external' => TRUE)),
      'class' => 'sift-caption',
    ),
  );

  if ($response->isOk()) {
    $integration = variable_get('commerce_siftscience_integration');

    $columns[] = array(
      'data' => l(t('User score: !sift_score', array(
        '!sift_score' => '<span>' . $response->body['score'] * 100 . '</span>',
      )), 'https://siftscience.com/console/users/' . $uid, array(
        'external' => TRUE,
        'html' => TRUE,
      )),
      'class' => 'sift-data sift-score',
    );

    $column = '';
    if (isset($response->body['latest_label']['is_bad'])) {
      if (
        !empty($integration['feedback'])
        && variable_get('commerce_siftscience_rest_key')
        && user_access('label users')
      ) {
        if ($response->body['latest_label']['is_bad'] == TRUE) {
          $img_path = drupal_get_path('module', 'commerce_siftscience') . '/images/label-bad.png';
        }
        else {
          $img_path = drupal_get_path('module', 'commerce_siftscience') . '/images/label-not-bad.png';
        }
        $image = theme('image', array('path' => $img_path));
        $column .= l($image, 'user/' . $uid . '/siftscience-unlabel', array(
          'html' => TRUE,
          'attributes' => array('title' => t('Unlabel user')),
        ));
      }
      $column .= l(t('User label: !sift_label', array(
        '!sift_label' => '<span>' . (($response->body['latest_label']['is_bad'] === TRUE) ? t('bad') : t('not bad')) . '</span>',
      )), 'https://siftscience.com/console/users/' . $uid, array(
        'external' => TRUE,
        'html' => TRUE,
      ));
    }
    elseif (
      !empty($integration['feedback'])
      && variable_get('commerce_siftscience_rest_key')
      && user_access('label users')
    ) {
      $img_path = drupal_get_path('module', 'commerce_siftscience') . '/images/label-bad.png';
      $image = theme('image', array('path' => $img_path));
      $column .= l($image, 'user/' . $uid . '/siftscience-label/1', array(
        'html' => TRUE,
        'attributes' => array('title' => t('Label user as BAD')),
      ));
      $img_path = drupal_get_path('module', 'commerce_siftscience') . '/images/label-not-bad.png';
      $image = theme('image', array('path' => $img_path));
      $column .= l($image, 'user/' . $uid . '/siftscience-label/0', array(
        'html' => TRUE,
        'attributes' => array('title' => t('Label user as NOT BAD')),
      ));
    }

    $columns[] = array(
      'data' => $column,
      'class' => 'sift-data sift-label',
    );

    $columns[] = array(
      'data' => t('Recommended action: !decision', array(
        '!decision' => '<span>' . t($decision) . '</span>',
      )),
      'class' => 'sift-data sift-decision',
    );

    $columns[] = array(
      'data' => l(t('more info &raquo;'), 'https://siftscience.com/sift-edu/prevent-fraud/basics-manual-review', array(
        'external' => TRUE,
        'html' => TRUE,
      )),
      'align' => 'right',
    );
  }
  else {
    $columns[] = array(
      'data' => t('Error @error_code calling Sift Science API: @error_message', array(
        '@error_code' => $response->apiStatus,
        '@error_message' => $response->apiErrorMessage,
      )),
    );
  }

  $element['commerce_siftscience'] = array(
    '#markup' => theme('table', array(
      'rows' => array($columns),
      'attributes' => array(
        'class' => array('sift-science', 'sift-decision-' . $decision),
      ),
    )),
    '#weight' => -50,
  );

  drupal_add_css(drupal_get_path('module', 'commerce_siftscience') . '/includes/commerce_siftscience.css');

  return $element;
}
